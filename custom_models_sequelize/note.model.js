const sequelize = require("./../custom_db/database-obj");
const { DataTypes } = require("sequelize");
// const { Sequelize } = require("sequelize");

console.log( "Let's get this show on the road!!!\n\n" );

// =============================================
// MODEL CREATION PHASE FOR MAPPING
// https://sequelize.org/master/manual/model-basics.html#model-definition
// =============================================

const Note = sequelize.define( "notes" , {

  // note: Sequelize.TEXT
  note: {
    type: DataTypes.TEXT
    , allowNull: false
  }

  // , tag: Sequelize.STRING
  , tag: {
    type: DataTypes.STRING
    , allowNull: false
  }

} );

module.exports = Note;