// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// =============================================
const { NODE_ENV , PORT } = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

// =============================================
// BASE SETUP
// =============================================
const express = require( 'express' );
const app = express();

// =============================================
// IMPORT DB MIDDLEWARE AUTHENTICATION PROMISE
// =============================================
const connectDB = require("./custom_auth_promise/authentication");

// =============================================
// ROUTER SETUP
// =============================================
const indexRouter = require("./routes/indexEndpoints");
const notesRouter = require("./routes/notesEndpoints");

// =============================================
// USE PARAMS - FINER DETAILS
// =============================================
app.use((req, res, next) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Content-Type', 'application/json');
  next();
}); 

// app.use( bodyParser.json() );
app.use( express.json() ); // for parsing application/json
app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

// =============================================
// APPLY THE ROUTES TO THE APP
// =============================================
app.use( "/" , indexRouter );
app.use( "/notes" , notesRouter );

// =============================================
// DB and BACKEND SERVERS INSTANTIATION
// =============================================
connectDB().then( async () => {
  
  await app.listen( PORT , () => { 

    // console.log( `Server is running on port ${PORT}` ); 
    // console.log( "PORT" , PORT );
      
    if( IS_PRODUCTION_NODE_ENV === true ){
      console.log( `Backend is running, baby.`);
    }
    if( IS_DEVELOPMENT_NODE_ENV === true ){
      console.log( `Production backend has started at http://127.0.0.1:${PORT} .`);
    }
    if( IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
      console.log(`Development backend is now testing at port ${PORT} . Run and visit your frontend!`);
    }
    
  } ); // PORT, hostname, backlog (null), cb 

} );