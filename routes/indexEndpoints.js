// =============================================
// BASE SETUP
// =============================================
const express = require("express");
const indexRouter = express.Router();

// =============================================
// ROUTING ASSIGNMENTS
// =============================================
indexRouter.get( "/" , ( req, res ) => {
  
  // TODO: retrieve index page(s)
  res.status(200).json( { message : "This is the plain-jane Index Page." } );

  // TEST WITH...
  // http://127.0.0.1:5000/

} );

module.exports = indexRouter;