// =============================================
// BASE SETUP
// =============================================
const express = require("express");
const notesApiRouter = express.Router();

// =============================================
// OLD DATABASE SETUP
// OLD QUERY MODIFIERS
// =============================================
// const Sequelize = require("sequelize");
// const Op = Sequelize.Op;

// =============================================
// QUERY MODIFIERS
// https://sequelize.org/master/manual/getting-started.html#tip-for-reading-the-docs
// =============================================
const { Op } = require("sequelize");

// =============================================
// IMPORT MODEL FOR INTERFACING WITH DB
// =============================================
const Note = require("./../custom_models_sequelize/note.model");

// =============================================
// ROUTING ASSIGNMENTS
// =============================================
notesApiRouter.get( "/" , ( req, res ) => {
  
  // res.status(200).json( { message : "This is the API Page." } );

  // TODO: retrieve all records
    
  Note.findAll()
  .then( notes => {
    // console.log( '>>>>>> All record: ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } ); 
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes

} );


notesApiRouter.post( "/" , ( req, res ) => {

  const { note, tag } = req.body;
  
  // TODO: create ONE record
  
  Note.create( { note, tag } )
  .then( newnote => {
    // console.log( '>>>>>> New record added: ' , newnote );
    // return res.status(200).json( newnote ); 
    return res.status(200).json( { message : "New record has been added" } );
  } )
  .catch( ( err ) => { 
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes
  /*
  {
    "note" : "Take the bicycle out for a long ride." 
    ,"tag" : "health"
  } 
  */

} );


notesApiRouter.put( "/:id" , ( req, res ) => {

  const id = parseInt( req.params.id );

  // TODO: update one record 
  
  const { note, tag } = req.body;
  
  Note.findByPk( id )
  .then( record => {
    return record.update( { note, tag } )
  } )
  .then( note => {
    // console.log( '>>>>>> Record updated: ' , note );
    return res.status(200).json( note );
    // return res.status(200).json( { message : "Record has been updated" } );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/7

  /*
  {
    "note" : "Pushback the sauce and instead prepare some fresh hashbrowns." 
    ,"tag" : "health"
  }
  */

} );


notesApiRouter.delete( "/:id" , ( req, res ) => {
  
  // TODO: find and delete note by id
  
  const id = parseInt( req.params.id );
  
  Note.findByPk(id)
  .then( ( note ) => {
    // console.log( '>>>>>> Record deleted: ' , note );
    return note.destroy( { where: { id : id } } );
  } )
  .then( () => {
    return res.status(200).json( { message: "Record has been deleted." } );
  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Error deleting record." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/2

} );


notesApiRouter.get( "/search" , ( req, res ) => {

  // const id = parseInt( req.query.id );
  const note = req.query.note;
  const tag = req.query.tag;   

  // TODO: retrieve one record using WHERE and AND modifiers

  Note.findAll( {
    where: {
      [Op.and]: [
        { note: note } , { tag: tag }
      ]
    }
    // where: {
    //   note: note, 
    //   tag: tag
    // }
    // where: {
    //   id: id
    // }
  } )
  .then( notes => {
    console.log( '>>>>>> All records WHERE AND: ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // quickly convert vanilla text to escaped characters at 
  // either https://www.w3schools.com/jsref/jsref_escape.asp
  // or https://www.w3schools.com/jsref/jsref_encodeuri.asp

  // http://127.0.0.1:5000/notes/search?note=Slap%20together%20the%20ReactJS%20and%20sqlite3%20lessons%20for%20publication.&tag=webdev
  // http://127.0.0.1:5000/notes/search?tag=webdev
  // http://127.0.0.1:5000/notes/search?id=8

} );


notesApiRouter.get( "/either" , ( req, res ) => {

  const note = req.query.note;
  const tag = req.query.tag;

  // TODO: retrieve any matching records using OR modifier

  Note.findAll( {
    where: {
      [Op.or]: [
        { note: note } , { tag: tag }
      ]
    }
  } )
  .then( notes => {
    console.log( '>>>>>> All one condition OR another condition... : ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/either?note=Send%20more%20job%20applications.&tag=webdev&tag=other
  // http://127.0.0.1:5000/notes/either?note=Pick%20up%20some%20bread.&tag=career&tag=health
  

} );


notesApiRouter.get( "/exclude" , ( req, res ) => {

  const tag = req.query.tag;

  // TODO: retrieve any matching records using NOT modifier

  Note.findAll( {
    // where: {
      // tag: tag
      // tag: {
      //   [Op.not]: [].concat( tag )
      // }
    // }
    where: {
      tag: {
        [Op.not]: tag
      }
    }

  } )
  .then( notes => {
    console.log( '>>>>>> All records but NOT some condition... : ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/exclude?tag=webdev

} );


notesApiRouter.get( "/limitation" , ( req, res ) => {

  const tag = req.query.tag;

  // TODO: retrieve any matching records using NOT modifier

  Note.findAll( {
    
    limit: 2,

    where: {
      // tag: tag
      tag: {
        [Op.eq]: tag
      }
    } 
    
  } )
  .then( notes => {
    console.log( '>>>>>> All records with LIMIT applied... : ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/limitation?tag=webdev

} );

// TWO versions - GET one record
notesApiRouter.get( "/:id" , ( req, res ) => {

  const id = parseInt( req.params.id );

  // TODO: retrieve one record using WHERE modifier

  // Note.findAll( // version one 
  //   { 
  //     where: {
  //       id: id
  //     } 
  //   }
  // ) 
  Note.findAll( // version two
    { 
      where: {
        id: {
          [Op.eq]: id
        }
      } 
    }
  )
  .then( notes => {
    console.log( '>>>>>> One record: ' , notes );
    return res.status(200).json( notes );
  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    return res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/notes/6

  // SOURCE...
  // https://sequelize.org/master/manual/model-querying-basics.html#the-basics
  // https://sequelize.org/master/manual/model-querying-basics.html#applying-where-clauses

} );

module.exports = notesApiRouter;