const sequelize = require("./../custom_db/database-obj");
const Note = require("./../custom_models_sequelize/note.model");

// =============================================
// SEEDER DATA
// =============================================
const seederData = [
  { note: "Pick up some bread.", tag: "personal" }
  , { note: "Send more job applications.", tag: "career" }
  , { note: "Continue with the Sequelize ORM lessons.", tag: "webdev" }
  , { note: "Upload finished projects to GIT repositories.", tag: "webdev" }
  , { note: "Clean up around the house.", tag: "other" }
  , { note: "Pick up where I left off with GraphQL.", tag: "webdev" }
  , { note: "Prepare some more homemade tomato sauce.", tag: "personal" }
  , { note: "Send some gardening links to Jake ASAP.", tag: "other" }
  , { note: "Return to the Okta lesson.", tag: "webdev" }
  , { note: "Slap together the ReactJS and sqlite3 lessons for publication.", tag: "webdev" }
];

// quickly convert vanilla text to escaped characters at 
// either https://www.w3schools.com/jsref/jsref_escape.asp
// or https://www.w3schools.com/jsref/jsref_encodeuri.asp

const seed = async () => {
  
  // create the table(s), dropping [them] first if [they] already existed
  // await sequelize.sync( { force: true } );

  // check the current state of the table(s) in the database (which columns [they] have, what are their data types, etc.), then, perform the necessary changes in the table(s) to make [them] match the model
  // await sequelize.sync({ alter: true });

  // create the table(s) if [they] don't exist (and do nothing if [they] already exist)
  await sequelize.sync();
  
  console.log(">>>>> All models were synchronized!\n\n");

  try {
    await Note.bulkCreate( seederData );
    console.log( ">>>>> DB has been seeded!\n\n" );
  }
  catch (error) {
    console.error( "Failed to seed: \n\n" , error )
    await sequelize.close();
  }

};

seed();

// =============================================
// TEST THE DATABASE IN A SEPARATE TERMINAL WINDOW
// =============================================
/*
sqlite3 database.sqlite3
.show
.headers ON
.mode column
SELECT * FROM notes;
.exit  
*/