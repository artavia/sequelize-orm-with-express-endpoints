# Sequelize Orm With Express Endpoints

## Description
Using Sequelize organically with the back end in order to get acquainted with simple workflow operations.

### Processing and/or completion date(s)
June 28, 2020 to June 29, 2020

## Purpose of this toy project
I am testing ExpressJS endpoints with POSTMAN with a simplified Sequelize ORM workflow. This presents a possibility for future use with either JWT, bcrypt, or the nodejs buffer api sprinkled on top, as well.

## Attributions
I took a little bit from two unique lessons in order to come up with my own formula. 

First, I borrowed some ideas from an exercise originally done by the folks at [StackAbuse](https://stackabuse.com/using-sequelize-orm-with-nodejs-and-express/ "link to stackabuse"). 

Then, I took some other ideas from another writeup done by the folks at [dev dot to](https://dev.to/getaclue/how-to-install-sequelize-in-expressjs-app-with-sqlite-4p63 "link to writeup by getaclue at dev dot to"). 

## Arrangement and Organization
With the two lessons under my belt I, then, sought to separate out backend logic from database modeling and vice-versa. I am happy with the results, hence, if there is anything to be learned from this lesson, you will find plenty.

## Addressing Archaicized Versions
In order to curb any mismatches with older versions of Sequelize (specifically), I prefer the latest version when installing packages from the onset. 

```sh
  $ yarn add sequelize@latest
  # or
  $ npm install --save sequelize@latest
```

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the [ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial"). Similar to the last few posts, I am getting acquainted with some of the **major and minor themes**. Sequelize is a **major** theme and it is quite cool because of its versatility. While I use it today for communication with SQLite3 that can easily change to any other more practical technologies such as MySQL or POSTGRES-SQL. 

## God bless!

That is all.

I thank you for your interest.